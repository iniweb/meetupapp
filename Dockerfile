FROM alpine

COPY gopath/bin/meetupapp /go/bin/meetupapp

ENTRYPOINT /go/bin/meetupapp
